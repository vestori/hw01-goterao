//
//  FloridaParksTableViewController.m
//  HW01-GoteraO
//
//  Created by Orlando Gotera on 11/9/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "FloridaParksTableViewController.h"
#import "ParkCellTableViewCell.h"
#import "ParkDetailsTableViewController.h"

@interface FloridaParksTableViewController ()
@property(nonatomic, strong) NSDictionary * parks;
@property(nonatomic, strong) NSArray * keys;
@end

@implementation FloridaParksTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *path = [[NSBundle mainBundle]pathForResource:@"parkInformation" ofType:@"plist"];
    
    //grab contents of plist and save it to our dictionary
    
    self.parks = [NSDictionary dictionaryWithContentsOfFile:path];
    
    self.keys = [[self.parks allKeys]sortedArrayUsingSelector:@selector(compare:)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.keys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"parkCell";
    ParkCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSString *key = self.keys[indexPath.section];
    NSArray *keyValues = self.parks[key];
 
    cell.parkName.text = keyValues[0];
    [cell.parkName setFont:[UIFont systemFontOfSize:25]];
    [cell.parkLocation setFont:[UIFont systemFontOfSize:15]];
    cell.parkLocation.text = keyValues[1];
    cell.parkImage.image = [UIImage imageNamed:keyValues[4]];
    
    return cell;
    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    if ([[segue identifier] isEqualToString:@"parkDetails"]) {
        ParkDetailsTableViewController *detailVC = [segue destinationViewController];
        NSIndexPath *myIndexPath = [self.tableView indexPathForSelectedRow];
        //        StateInfo *item = [self.usStates objectAtIndex:myIndexPath.row];
        NSString *key = self.keys[myIndexPath.section];
        NSArray *keyValues = self.parks[key];
        detailVC.myParkName = keyValues[0];
        detailVC.myParkImage = keyValues[4];
        detailVC.myParkDescription = keyValues[6];
        detailVC.myParkLocation =keyValues[1];
        detailVC.myParkFoundedDate = keyValues[5];
        detailVC.myParkPhone = keyValues[2];
        detailVC.myParkAddress = keyValues[3];
        
    }
}

@end
