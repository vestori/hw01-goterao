//
//  ParkCellTableViewCell.m
//  HW01-GoteraO
//
//  Created by Orlando Gotera on 11/11/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "ParkCellTableViewCell.h"

@implementation ParkCellTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
