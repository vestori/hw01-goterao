//
//  ParkCellTableViewCell.h
//  HW01-GoteraO
//
//  Created by Orlando Gotera on 11/11/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParkCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *parkImage;
@property (weak, nonatomic) IBOutlet UILabel *parkName;
@property (weak, nonatomic) IBOutlet UILabel *parkLocation;



@end
