//
//  ParkDetailsTableViewController.m
//  HW01-GoteraO
//
//  Created by Orlando Gotera on 11/11/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import "ParkDetailsTableViewController.h"

@interface ParkDetailsTableViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *lblParkImage;
@property (weak, nonatomic) IBOutlet UILabel *lblParkDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblParkFoundedDate;
@property (weak, nonatomic) IBOutlet UILabel *lblParkLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblParkAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblParkPhone;


@end

@implementation ParkDetailsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.myParkName;
    self.lblParkImage.image = [UIImage imageNamed:self.myParkImage];
    self.lblParkDescription.text = self.myParkDescription;
    self.lblParkFoundedDate.text = self.myParkFoundedDate;
    self.lblParkLocation.text = self.myParkLocation;
    self.lblParkAddress.text = self.myParkAddress;
    self.lblParkPhone.text = self.myParkPhone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
