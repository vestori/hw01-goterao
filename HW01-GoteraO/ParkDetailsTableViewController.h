//
//  ParkDetailsTableViewController.h
//  HW01-GoteraO
//
//  Created by Orlando Gotera on 11/11/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParkDetailsTableViewController : UIViewController
@property (strong, nonatomic) NSString * myParkName;
@property (strong, nonatomic) NSString * myParkImage;
@property (strong, nonatomic) NSString * myParkDescription;
@property (strong, nonatomic) NSString * myParkFoundedDate;
@property (strong, nonatomic) NSString * myParkLocation;
@property (strong, nonatomic) NSString * myParkAddress;
@property (strong, nonatomic) NSString * myParkPhone;
@end
